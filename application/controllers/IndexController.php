<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $albums = new Application_Model_DbTable_Albums();
        $this->view->albums = $albums->fetchAll();
    }

    public function ajouterAction()
    {
        $form = new Application_Form_Album();
        $form->envoyer->setLabel('Ajouter');
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($form->isValid($formData)) {
                $artiste = $form->getValue('artiste');
                $titre = $form->getValue('titre');
                $albums = new Application_Model_DbTable_Albums();
                $albums->ajouterAlbum($artiste, $titre);

                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        }
    }

    public function modifierAction()
    {
        $form = new Application_Form_Album();
        $form->envoyer->setLabel('Sauvegarder');
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($form->isValid($formData)) {
                $id = $form->getValue('id');
                $artiste = $form->getValue('artiste');
                $titre = $form->getValue('titre');
                $albums = new Application_Model_DbTable_Albums();
                $albums->modifierAlbum($id, $artiste, $titre);

                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else {
            $id = $this->_getParam('id', 0);
            if ($id > 0) {
                $albums = new Application_Model_DbTable_Albums();
                $form->populate($albums->obtenirAlbum($id));
            }
        }
    }

    public function supprimerAction()
    {
        if ($this->getRequest()->isPost()) {
            $supprimer = $this->getRequest()->getPost('supprimer');
            if ($supprimer == 'Oui') {
                $id = $this->getRequest()->getPost('id');
                $albums = new Application_Model_DbTable_Albums();
                $albums->supprimerAlbum($id);
            }
            $this->_helper->redirector('index');
        } else {
            $id = $this->_getParam('id', 0);
            $albums = new Application_Model_DbTable_Albums();
            $this->view->album = $albums->obtenirAlbum($id);
        }
    }

    public function countriesAction()
    {
        $continents = array();

        $client = new Zend_Soap_Client('http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL');
        $result = $client->ListOfContinentsByName();
        $conti=$result->ListOfContinentsByNameResult->tContinent;
        foreach ($conti as $a){


            $continent = new Application_Model_DbTable_Continent();
            $continent->setName($a->sName);
            $continent->setCode($a->sCode);
            array_push($continents, $continent);
        }

        //l'envoie les donneés vers front
        $this->view->data = $continents;

                            /*********** liste of countries by continent **********/
    $listPays = array();



    
    $client = new Zend_Soap_Client('http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL');
    $time = microtime(true);
    $client->FullCountryInfoAllCountries()
                    ->FullCountryInfoAllCountriesResult
                    ->tCountryInfo;
    $time = microtime(true) - $time;

    var_dump($time);

    $conti = $client->FullCountryInfoAllCountries()
                    ->FullCountryInfoAllCountriesResult
                    ->tCountryInfo;
    foreach ($conti as $a){
        $pays = new Application_Model_DbTable_Pays();
        $pays->setName($a->sName);
        $pays->setCountryFlag($a->sCountryFlag);
        $pays->setISOCode($a->sISOCode);
        array_push($listPays, $pays);
        }$this->view->dataPays = $listPays;






if(isset($_GET['cont'])){
    $listPays=array();
    foreach ($conti as $a){
        if($a->sContinentCode==$_GET['cont']){
            $pays = new Application_Model_DbTable_Pays();
            $pays->setName($a->sName);
            $pays->setCountryFlag($a->sCountryFlag);
            $pays->setISOCode($a->sISOCode);
            array_push($listPays, $pays);
        }
         }
}

if(isset($_POST['search'])){
    $listPays=array_filter($listPays,function($a){
        return strpos(strtoupper($a->getName()),strtoupper($_POST['search']))===0;
    }
);}


        //l'envoie les donneés vers front
        $this->view->dataPays = $listPays;
            }

    public function paysInfoAction()
    {


        if(isset($_GET['nom'])){

            $infoPays = array();
            $languages = array();

            $client = new Zend_Soap_Client('http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL');
            $a = new \stdClass();
            $a->sCountryISOCode = $_GET['nom'];
            $result = $client->FullCountryInfo($a);
            $conti=$result->FullCountryInfoResult;
                    $pays = new Application_Model_DbTable_Pays();
                    $pays->setISOCode($conti->sISOCode);
                    $pays->setName($conti->sName);
                    $pays->setCountryFlag($conti->sCountryFlag);
                    $pays->setCapitalCity($conti->sCapitalCity);
                    $pays->setPhoneCode($conti->sPhoneCode);
                    $pays->setCurrencyISOCode($conti->sCurrencyISOCode);

                    if(isset($conti->Languages->tLanguage)){
                        $total = count((array)$conti->Languages->tLanguage);
                        if($total==2){
                            array_push($languages,$conti->Languages->tLanguage->sName);
                          }else if($total>2){
                        foreach ($conti->Languages->tLanguage as $a){
                        array_push($languages,$a->sName);
                        }
                    }else{
                        array_push($languages,"not found!");
                    }
                    $pays->setLanguages($languages);
                    array_push($infoPays, $pays);

                    //l'envoie les donneés vers front
                    $this->view->dataInfosPays = $infoPays;
    }
    }
    }}