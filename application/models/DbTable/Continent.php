<?php

class Application_Model_DbTable_Continent extends Zend_Db_Table_Abstract
{

    public $_sCode;
    public $_sName;
    //public Pays $_sName=array();

    public function getCode() {
        return $this->__sCode;
    }

    public function setCode(string $_sCode) {
        $this->_sCode = $_sCode;
    }



    public function getName() {
        return $this->_sName;
    }

    public function setName(string $_sName) {
        $this->_sName = $_sName;
    }


}

