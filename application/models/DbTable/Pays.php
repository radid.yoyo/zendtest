<?php

class Application_Model_DbTable_Pays extends Zend_Db_Table_Abstract
{
    public $_sName;
    public $_sISOCode;
    public $_sCapitalCity;
    public $_sCountryFlag;
    public $_sPhoneCode;
    public $_sCurrencyISOCode;
    public $_language = array();

/*** drapeau url */
    public function getCountryFlag() {
        return $this->_sCountryFlag;
    }

    public function setCountryFlag(string $_sCountryFlag) {
        $this->_sCountryFlag = $_sCountryFlag;
    }


/*** nom de pays */
    public function getName() {
        return $this->_sName;
    }

    public function setName(string $_sName) {
        $this->_sName = $_sName;
    }

/*** code iso */
    public function getISOCode() {
        return $this->_sISOCode;
    }

    public function setISOCode(string $_sISOCode) {
        $this->_sISOCode = $_sISOCode;
    }
/*** capitale */
    public function getCapitalCity() {
        return $this->_sCapitalCity;
    }

    public function setCapitalCity(string $_sCapitalCity) {
        $this->_sCapitalCity = $_sCapitalCity;
    }


/*** phone code */

    public function getPhoneCode() {
        return $this->_sPhoneCode;
    }

    public function setPhoneCode(string $_sPhoneCode) {
        $this->_sPhoneCode = $_sPhoneCode;
    }


/***** ISO CODE *******/

    public function getCurrencyISOCode() {
        return $this->_sCurrencyISOCode;
    }

    public function setCurrencyISOCode(string $_sCurrencyISOCode) {
        $this->_sCurrencyISOCode = $_sCurrencyISOCode;
    }


/***** languages  ******/

public function getLanguages() {
    return $this->$_language;
}

public function setLanguages($_language) {
    $this->_language = $_language;
}
}
